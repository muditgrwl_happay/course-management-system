# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Courses', '0005_auto_20180713_0511'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ('times_requested',)},
        ),
    ]
