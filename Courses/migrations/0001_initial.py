# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Courses',
            fields=[
                ('course_id', models.CharField(max_length=8, unique=True, serialize=False, primary_key=True)),
                ('course_name', models.CharField(max_length=200)),
                ('is_available', models.BooleanField(default=True)),
                ('seats_available', models.IntegerField(default=10)),
                ('seats_occupied', models.IntegerField(default=0)),
                ('times_requested', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('student_id', models.CharField(max_length=10, unique=True, serialize=False, primary_key=True)),
                ('student_name', models.CharField(max_length=200)),
                ('student_email', models.CharField(max_length=200)),
                ('course', models.ForeignKey(to='Courses.Courses')),
            ],
        ),
    ]
