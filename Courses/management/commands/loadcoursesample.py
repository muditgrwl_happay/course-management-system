import csv
from django.core.management.base import BaseCommand
from Courses.models import Course


# Class for loading data into Course database
class Command(BaseCommand):

    def handle(self, *args, **options):
        print "Loading CSV"
        csv_path = "./sampledata_Course.csv"
        csv_file = open(csv_path, 'rb')
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            obj = Course.objects.create(
                course_name=row['course_name'],
                course_id=row['course_id'],
            )
            print obj
