import csv
from django.core.management.base import BaseCommand
from Courses.models import Student


# Class for loading data into Student database
class Command(BaseCommand):

    def handle(self, *args, **options):
        print "Loading CSV"
        csv_path = "./sampledata_Student.csv"
        csv_file = open(csv_path, 'rb')
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            obj = Student.objects.create(
                student_name=row['student_name'],
                student_id=row['student_id'],
                student_email=row['student_email'],
            )
            print obj
