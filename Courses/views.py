# from django.shortcuts import render
from django.views.generic import View
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.db import IntegrityError
from .models import Course, Student


# Create your views here.

class Home(View):

    def get(self, request):
        return HttpResponse('<h1> Course Management System </h1> </br> </br> <h2> Home Page!!! </h2></br> </br> '
                            '<ul><h3> Features: </h3>'
                            '<li> View Courses <li> View Course Stats <li> View Available Courses <li> Enroll for '
                            'courses </ul>', status=200)


# GET method for viewing all courses in the database
class ViewCourses(View):
    def get(self, request):
        try:
            course_list = Course.objects.all()
            result = ['<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' % (
                'Course Name', 'Course ID', 'Seats Available', 'Seats Occupied', 'Availability')]
            for course in course_list:
                result.append('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' %
                              (course.course_name, course.course_id, course.seats_available, course.seats_occupied,
                               course.is_available))

            return HttpResponse('<table>%s</table>' % '\n'.join(result), status=200)

        except Exception as exception:
            str(exception)
            return HttpResponse('Internal Server Error', status=500)


# GET method for viewing course statistics , in descending order of popularity
class ViewCourseStats(View):
    def get(self, request):
        try:
            course_list = Course.objects.all()
            result = ['<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' % (
                'Course Name', 'Course ID', 'Availability', 'Popularity', 'Seats Occupied')]
            for course in course_list:
                result.append('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' %
                              (course.course_name, course.course_id, course.is_available, course.times_requested,
                               course.seats_occupied))

            return HttpResponse('<table>%s</table>' % '\n'.join(result), status=200)

        except Exception as exception:
            str(exception)
            return HttpResponse('Internal Server Error', status=500)


# GET method for viewing the courses available to a particular student (excludes courses student is already enrolled in)
class AvailableCourses(View):
    def get(self, request):
        try:
            sid = request.GET['student_id']
            student = Student.objects.get(student_id=sid)
            enrolled_courses = student.course.all()
            flag = []

            # Tailoring response according to pre-existing courses if any
            if enrolled_courses:
                for course in enrolled_courses:
                    flag.append(course.course_id)
                a = flag[0]
                b = flag[1]
                c = flag[2]
                course_list = Course.objects.exclude(course_id__in=[a, b, c])
            else:
                course_list = Course.objects.all()

            result = ['<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>' % ('Course Name',
                                                                                'Course ID',
                                                                                'Seats Occupied',
                                                                                'Seats Available')]
            for course in course_list:
                if course.is_available:
                    result.append('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>' %
                                  (course.course_name, course.course_id, course.seats_occupied,
                                   course.seats_available))
                else:
                    continue
            if not result:
                return HttpResponse(str(student.student_name) + ' ' + str(student.student_id) +
                                    ' No courses available!!! Check Later...', status=200)
            else:
                return HttpResponse('<table>%s</table>' % '\n'.join(result), status=200)
        except ObjectDoesNotExist:
            return HttpResponse('Student does not exist in database', status=500)

        except MultiValueDictKeyError:
            return HttpResponse('Multiple values returned in the dictionary', status=500)


'''POST method for Enrolling in a course. Each student has to enroll for exactly 3 courses. Validations on student_id, 
course_id(s) and pre-opted courses along with availability changes to course after registration have also been 
implemented'''


class EnrollForCourse(View):
    def post(self, request):
        try:
            course_list = Course.objects.all()
            student_list = Student.objects.all()

            post_req = request.POST
            student_id = post_req['student_id']
            course_id_1 = post_req['course_id_1']
            course_id_2 = post_req['course_id_2']
            course_id_3 = post_req['course_id_3']
            requested_courses = [course_id_1, course_id_2, course_id_3]

            sid_check = 0
            cid_check = 0

            # Validating student_id and course_id
            for student in student_list:
                if student_id == student.student_id:
                    sid_check = 1
                else:
                    continue

            if sid_check == 0:
                return HttpResponse('Invalid Student ID. Please verify entry or register if new user', status=200)
            else:
                for course in course_list:
                    for cid in requested_courses:
                        if cid == course.course_id:
                            cid_check += 1
                        else:
                            continue

            if cid_check != 3:
                return HttpResponse('Invalid Course ID. Please verify entry and retry', status=200)

            # Checking choices against pre-registered choices
            set_student = Student.objects.get(student_id=student_id)
            opted_courses = []

            for cid in requested_courses:
                opted_courses.append(Course.objects.get(course_id=cid))

            set_student_courses = set_student.course.all()

            if set_student_courses.count() != 0:
                return HttpResponse('You are already enrolled in several courses. Please finish them up first...',
                                    status=200)

            for select_course in set_student_courses:
                if select_course in opted_courses:
                    return HttpResponse('You have entered course(s) which you are already registered for. '
                                        'Please verify!', status=200)

            # Registering choices for set_student
            for choice in opted_courses:
                choice.times_requested += 1
                choice.save()

            for choice in opted_courses:
                if choice.seats_available > 0:
                    set_student.course.add(choice)
                    choice.seats_available -= 1
                    choice.seats_occupied += 1
                    set_student.save()
                    choice.save()
                    if (choice.seats_occupied == 10) and (choice.seats_available == 0):
                        choice.is_available = False
                        choice.save()
                        print(str(student_id) + ' opted for ' + str(choice.course_name))
                else:
                    return HttpResponse(str(choice.course_name) + ' currently filled', status=200)
            return HttpResponse('Opted Successfully!', status=200)

        except IntegrityError:
            return HttpResponse("Roll No already exists!", status=500)
        except MultiValueDictKeyError:
            return HttpResponse("Incomplete form submitted. Please enter all 3 choices.", status=500)
