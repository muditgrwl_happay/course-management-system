from __future__ import unicode_literals
from django.db import models


# Model class for Course object
class Course(models.Model):
    course_id = models.CharField(max_length=8, unique=True, primary_key=True)
    course_name = models.CharField(max_length=200)
    is_available = models.BooleanField(default=True)
    seats_available = models.IntegerField(default=10)
    seats_occupied = models.IntegerField(default=0)
    times_requested = models.IntegerField(default=0)

    def __unicode__(self):
        return str(self.course_name) + ' ' + str(self.course_id)

# Meta for ordering the Courses according to their popularity (reflected by times_requested)
    class Meta:
        ordering = ('-times_requested',)


# Model class for Student object
class Student(models.Model):
    student_id = models.CharField(max_length=10, unique=True, primary_key=True)
    student_name = models.CharField(max_length=200)
    student_email = models.CharField(max_length=200)
    course = models.ManyToManyField(Course, blank=True)

    def __unicode__(self):
        return str(self.student_name) + ' ' + str(self.student_id)