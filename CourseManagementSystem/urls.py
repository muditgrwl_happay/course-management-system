from django.conf.urls import include, url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from Courses.views import ViewCourses, AvailableCourses, ViewCourseStats, EnrollForCourse, Home




urlpatterns = [
    # Examples:
     url(r'^$', csrf_exempt(Home.as_view()), name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^courses/$', csrf_exempt(ViewCourses.as_view()),
        name="CourseManagementSystem-ViewCourses"),
    url(r'^available_courses/$', csrf_exempt(AvailableCourses.as_view()),
        name="CourseManagementSystem-AvailableCourses"),
    url(r'^course_stats/$', csrf_exempt(ViewCourseStats.as_view()),
        name="CourseManagementSystem-CourseStats"),
    url(r'^enroll_for_course/$', csrf_exempt(EnrollForCourse.as_view()),
        name="CourseManagementSystem-Enroll"),
]
