#This contains the description of the app Course Management System

The app Lists the Courses available in the database, Lists the Statistics for all courses in the database, Lists the Courses Available to the student mapped to the input student ID and allows students to opt for available courses.

GET and POST queries are implemented using the generice views in Django framework. The databases of Courses and Students are preloaded in the application.

##Getting Started

Cloning a repository to clone the repository in your local system run: 
git clone https://muditgrwl_happay@bitbucket.org/muditgrwl_happay/course-management-system.git

##Prerequisites

- Djnago 1.8
- sqlite (available in the Django framework) 

##Installing django

pip install django==1.8

##Running the tests

*make sure to run the command: python manage.py makemigrations to check for any changes in models*
*to apply any migrations run the command: python manage.py migrate to apply any migrations*

- Run the command - python manage.py runserver 
- Go to adress http://127.0.0.1:8000/admin/ to access admin
    - username- MUDIT
    - password- 666seals
- To List the **courses** in the database hit the url http://127.0.0.1:8000/courses/
- To List the **course statistics** hit the url http://127.0.0.1:8000/course_stats/
- To List the **available courses** for a **particular student** hit the url                                   http://127.0.0.1:8000/available_courses/?student_id=HPYxxxx
  {where xxxx is a 4 digit number. It is to note that xxxx is in the range 0001-0050}
- To **Enroll for courses**, hit the POST Request at http://127.0.0.1:8000/enroll_for_course in the                 POSTMAN client with body parameters as 'student_id', 'course_id_1', 'course_id_2' and 'course_id_3' 

###Points to note
- There are 10 courses in the database and 50 students
- New courses and new students can only be added through the admin
- A student can opt for exactly 3 courses
- No feature for modifying opted courses